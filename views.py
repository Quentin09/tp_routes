from flask import render_template, flash, redirect
from app import app

@app.route('/', methods=('GET',) )
def index():
    return render_template("index.html",
                           title = "Home",
                          )

@app.route('/truc', methods=('GET',) )
def perdu():
    return "Tu t'es perdu", 404

@app.route('/mes_listes', methods=('GET',) )
def mes_listes():
    return "Page non encore implémentée", 501

@app.route('/mes_categories', methods=('GET',) )
def mes_categories():
    return "Page non encore implémentée", 501

@app.route('/mes_magasins', methods=('GET',) )
def mes_magasins():
    return "Page non encore implémentée", 501

@app.route('/mes_recettes', methods=('GET',) )
def mes_recettes():
    return "Page non encore implémentée", 501
